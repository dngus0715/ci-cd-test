
using System;
using UnityEditor;
using UnityEngine;
using System.Linq;

public class BuildScript
{
    public static void PerformBuild()
    {
        string buildPath = Environment.GetEnvironmentVariable("BUILD_PATH");

        Console.Write(buildPath);

        if(string.IsNullOrWhiteSpace(buildPath))
        {
            throw new ArgumentNullException(paramName:"BUILD_PATH");
        }
        
        string[] defaultScene = EditorBuildSettings.scenes
            .Where(s => s.enabled)
            .Select(s => s.path)
            .ToArray();

        BuildTarget buildTarget = EditorUserBuildSettings.activeBuildTarget;

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = defaultScene,
            locationPathName = buildPath, // Output path
            target = buildTarget, // Target platform
            options = BuildOptions.None
        };

        BuildPipeline.BuildPlayer(buildPlayerOptions);
    }
}
