#!/bin/bash

# Path to Unity Editor. Replace with your actual Unity editor path

export BUILD_TARGET=$BUILD_TARGET_ARG

UNITY_PATH="/media/vhexlab/D/Unity/2022.3.20f1/Editor/"

UNITY_EXEC="./Unity"

cd $UNITY_PATH || exit


# Path to your Unity project
PROJECT_PATH="/media/vhexlab/D/Jenkins/workspace/ci-test/ci-ci-test"

# Log file
LOG_PATH="$PROJECT_PATH/unity_build.log"

# Execute the build
if ! "$UNITY_EXEC" -batchmode -nographics -silent-crashes -logFile "$LOG_PATH" -projectPath "$PROJECT_PATH" -executeMethod BuildScript.PerformBuild -quit; then
    echo "Build Succeeded"
    exit 0
else
    echo "Build failed"
    exit 1
fi
